<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#catalogue {
	color: #777777;
	background-color: white;
	width: 43%;
	height: 1200px;
	float: left;
	margin: 0 auto;
	margin-left: 15%;
	padding-left: 5%;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="catalogue"></div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>