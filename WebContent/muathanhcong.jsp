<%@page import="java.util.Calendar"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.model.GioHang"%>
<%@page import="com.dieulinh.model.DonHang"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}
#xacnhan{
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width:70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	vertical-align: baseline;
}
.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 url(images/ico-tick.png) no-repeat 20px 13px;
	float: left;
}
.button {
	
	margin-left: calc(50% - 60px);
	margin-right: 5px;
	margin-bottom: 100px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
</style>
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="xacnhan">
		<div class="thongbao">
			Đặt hàng thành công, sản phẩm sẽ được giao cho bạn trong thời gian sớm nhất! 
		</div>
		<a href='index.jsp'><input type='submit' class='button' value='Tiếp tục mua hàng ⊱'></a>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>