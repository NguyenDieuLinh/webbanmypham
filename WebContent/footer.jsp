<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>footer</title>
<style>
#footer {
	background-color: #637b01;
	width: 70%;
	margin: auto;
	margin-left: 15%;
	margin-top: 0;
	float: left;
}

#footer p {
	font-size: 14px;
	color: #fff;
	text-shadow: 0 1px 0 rgba(0, 0, 0, 0.2);
	margin-left: 20px;
	margin-right: 20px;
}
</style>
</head>
<body>
	<div id="footer">
		<div style="float: left;">
			<p style="width: 100%;">
				<b>CHUYÊN CUNG CẤP SỈ & LẺ CÁC LOẠI MỸ PHẨM XÁCH TAY</b> <br>
				Địa chỉ: 40B - An Hòa - Hà Đông - Hà Nội <br> Điện thoại: 0913
				908 969 - 0166 9 3535 68 - Email: linhnd1208@gmail.com <br> <a
					href="#sitemap">Sitemap</a>

			</p>
		</div>
		<div style="float: right;">
			<p>
				Thanh toán an toàn <br> <img alt="" src="images/thanh-toan.png">
			</p>
		</div>
	</div>
</body>
</html>