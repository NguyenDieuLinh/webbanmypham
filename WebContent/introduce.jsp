<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Giới thiệu</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#introduce {
	color: #777777;
	background-color: white;
	width: 43%;
	height: 1200px;
	float: left;
	margin: 0 auto;
	margin-left: 15%;
	padding-left: 5%;
}
h1,h2,p{
	color:#8eb001;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="introduce">
		<h1>Giới thiệu</h1>
		<h2>Shop mỹ phẩm online</h2>
		<p style="margin-right: 50px;">
			Shop mỹ phẩm online cam kết phân phối mỹ phẩm chính hãng.<br><br>

<span style="font-weight: bold;">Mỹ phẩm Missha</span> là một trong những hãng mỹ phẩm khá phổ biến dành cho giới văn phòng tại Hàn Quốc. Đến nay, Missha đã có mặt trên thị trường hơn 30 nước trên thế giới.<br>

<br><span style="font-weight: bold;">MISSHA</span> thu hút sự chú ý của khách hàng nhờ vào sự đa dạng về chủng loại, sự phong phú về màu sắc, cá tính trong khuynh hướng mới, đặc biệt là thấu hiểu chính xác nhu cầu và đặc tính cơ thể của người Châu Á . Chính vì vậy Missha đang là 1 trong những hãng mỹ phẩm được đánh giá là cao cấp nhất của Hàn Quốc <br>



<br><span style="font-weight: bold;">Skin food</span> là một trong những hãng mỹ phẩm đầu tiên của Hàn Quốc sử dụng các nguyên liệu được chiết xuất từ thiên nhiên, đặc biệt là các loại rau củ quả, trái cây có trong tự nhiên như khoai tây, dưa leo, cà rốt, kiwi, sữa chua, cà chua, chuối, mật ong hay lô hội...nên Không gây kích ứng da đồng thời vẫn cung cấp đầy đủ dưỡng chất, cho bạn 1 làn da tươi sáng và khỏe mạnh.Skin food mới có mặt tại Việt Nam nhưng được giới trẻ rất ưa chuộng, giá cả hợp lý chất lượng tốt.<br>



<br><span style="font-weight: bold;">Tonymoly</span> là hãng mĩ phẩm thời trang nên luôn luôn được cải tiến không ngừng về mẫu mã , xu hướng thời trang , chất lượng sản phẩm đến giá cả hợp lý với người tiêu dùng để mang lại vẻ đẹp và sự hài lòng tốt nhất.Mục tiêu của Tonymoly là mang lại sự tiện lợi , chăm sóc từ đầu tới cuối , mang lại một vẻ đẹp thuần khiết để mỗi phụ nữ có thể tự cảm nhận và ý thức được việc gìn giữ một làn da khoẻ mạnh và đầy sức sống.<br>



<br><span style="font-weight: bold;">ELANCYL</span> - Chuyên gia chăm sóc thân thể Thon thả, mảnh mai, mịn màng, săn chắc… Trong suốt hơn 35 năm qua, Elancyl luôn là nhãn hiệu và là chuyên gia hàng đầu trong lĩnh vực làm ốm và chăm sóc thân thể. Các hoạt chất từ thiên nhiên Cafein, phloridzin, tiêu Sichuan, cúc tai chuột, dây thường xuân… đăng ký độc quyền các công thức mới Công thức độc đáo, khơi dậy nguồn cảm xúc Thể chất sản phẩm hoà hợp một cách hoàn hảo với làn da và nhịp sinh học của cơ thể bạn. Công thức được thiết kế phóng thích theo thời gian giúp các hoạt chất thấm sâu và phát huy tác dụng suốt ngày chỉ với một lần thoa duy nhất. Màu sắc và mùi hương của sản phẩm dẫn bạn vào một hành trình khám phá cảm xúc đầy bất ngờ thú vị… Hiệu quả thấy rõ sau 14 ngày Với Elancyl, chỉ sau 14 ngày sử dụng, bạn đã cảm nhận được hiệu quả, thậm chí có thể tự tin đo lường kết quả ấy! Các tiêu chuẩn khắt khe chứng nhận độ an toàn và hiệu quả Với mục tiêu mang đến cho bạn những gì tuyệt vời nhất, Elancyl luôn áp dụng những tiêu chuẩn khắt khe nhất của Công nghệ Dược đối với quy trình nghiên cứu, sản xuất và kiểm nghiệm sản phẩm.<br>



<br><span style="font-weight: bold;">Mỹ phẩm Avene</span> chuyên cung cấp những sản phẩm vừa phù hợp với nhu cầu chăm sóc hàng ngày vừa đáp ứng nhu cầu chuyên sâu của giới chuyên gia. Các sản phẩm của avene bao gồm dòng tri mụn, trị nám, ngăn ngừa lão hoá, đặc biệt còn phù hợp với làn da nhạy cảm nhất .<br>
		</p>
	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>