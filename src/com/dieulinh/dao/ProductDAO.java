package com.dieulinh.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.dieulinh.model.Product;

public class ProductDAO {

	private Connection con = ConfigJDBC.initConnection();

	public ArrayList<Product> getListProducts() {
		ArrayList<Product> list = new ArrayList<>();
		String sql = "SELECT * FROM product";

		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Product pr = new Product(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getString(7), rs.getString(8), 1);
				list.add(pr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Product> searchProduct(int id) {
		ArrayList<Product> list = new ArrayList<>();
		String sql = "SELECT * FROM product WHERE id = " + id;

		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Product pr = new Product(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getString(7), rs.getString(8), 1);
				list.add(pr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Product> getProductByName(String name) {
		ArrayList<Product> list = new ArrayList<>();
		String sql = "SELECT * FROM product WHERE name like '%" + name + "%'";

		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Product pr = new Product(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getString(7), rs.getString(8), 1);
				list.add(pr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Product> getProductByCategory(int category) {
		ArrayList<Product> list = new ArrayList<>();
		String sql = "SELECT * FROM product WHERE category = " + category;

		Statement st;
		try {
			st = con.createStatement();

			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Product pr = new Product(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getString(7), rs.getString(8), 1);
				list.add(pr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Product> sort(String dieuKien) {
		String sql = "";
		if (dieuKien.compareTo("Mặc định") == 0) {
			sql = "SELECT * FROM product";

		} else {
			if (dieuKien.compareToIgnoreCase("Mới nhất") == 0) {
				sql = "SELECT * FROM product ORDER BY datemodified DESC";

			} else {
				if (dieuKien.compareToIgnoreCase("Giá từ thấp tới cao") == 0) {
					sql = "SELECT * FROM product ORDER BY price";
					System.out.println("đến đây rồi này");
				} else {
					sql = "SELECT * FROM product ORDER BY price DESC";
				}
			}
		}
		ArrayList<Product> list = new ArrayList<>();

		Statement st;
		try {
			st = con.createStatement();

			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Product pr = new Product(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getInt(5),
						rs.getString(6), rs.getString(7), rs.getString(8), 1);
				list.add(pr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
}
