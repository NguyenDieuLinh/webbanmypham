<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#guide {
	background-color: white;
	width: 48%;
	height: 1200px;
	float: left;
	margin: 0 auto;
	color: #777777;
	margin-left: 15%;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="guide">
		<img alt="" src="images/guide.png" style="width: 100%; height: auto;">
	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>