<%@page import="com.dieulinh.toolkit.Convert"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.dieulinh.dao.ProductDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style>
#content {
	width: 48%;
	margin: 0 auto;
	float: left;
	margin-left: 15%;
	background-color: white;
	height: 1200px;
}

#content .product {
	margin-top: 20px;
	margin-bottom: 20px;
	width: 25%;
	margin-left: 25px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	padding: 10px;
	float: left;
	margin-right: 5px;
}

.button {
	float: left;
	margin-left: 25%;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
</style>
</head>
<body>

	<div id="content">
		<h3 style="color: #a1a1a1; margin-left: 10px;">Chuyên cung cấp mỹ
			phẩm online sỉ &amp; lẻ, Giao hàng toàn quốc</h3>

		<%
			int check = 0;
			ProductDAO proDao = new ProductDAO();
			ArrayList<Product> arr = new ArrayList<>();
			String categoryID = "";

			if (request.getParameter("categoryID") != null) {
				categoryID = request.getParameter("categoryID");
				arr = proDao.getProductByCategory(Integer.parseInt(categoryID));
				check = 1;
		%>
		<h3 style="color: red; margin-left: 10px;">
			Có
			<%=arr.size()%>
			sản phẩm trong category
		</h3>
		<%
			} else {
				if (request.getAttribute("searchArr") instanceof ArrayList) {
					arr = (ArrayList<Product>) request.getAttribute("searchArr");
					check = 2;
		%>
		<h3 style="text-transform: uppercase; color: red; margin-left: 10px;">
			Có
			<%=arr.size()%>
			sản phẩm tìm thấy
		</h3>
		<%
			} else {
					arr = proDao.getListProducts();
					check = 3;
		%>
		<h3 style="text-transform: uppercase; color: red; margin-left: 10px;">
			Sản phẩm khuyến mại
		</h3>
		<%
			}
			}
		%>
		<%
			for (Product p : arr) {
		%>
		<div class="product">
			<a href="detail.jsp?productId=<%=p.getId()%>"> <img alt=""
				src="images/<%=p.getImage()%>" width="90%" height="90%"
				style="margin-left: 5%; float: left;">
			</a>
			<h4 style="color: #8eb001; text-align: center;"><%=p.getName()%></h4>
			<h3 style="color: red;" align="center"><%=Convert.ToString(p.getPrice())%>
				₫
			</h3>
			<form action="ThemHangVaoGio?productId=<%=p.getId()%>" method='POST'>
				<button class="button">Mua hàng</button>
			</form>
		</div>
		<%
			}
		%>
	</div>
</body>
</html>