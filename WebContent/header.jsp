<%@page import="com.sun.xml.internal.txw2.Document"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>header</title>
<style>
#top {
	background-color: #637b01;
	width: 100%;
	height: 34px;
	margin: 0;
	padding: 0;
}

.col-full {
	width: 70%;
	margin: 0 auto;
}

#nav {
	z-index: 99;
	margin: 0;
	padding: 0;
	list-style: none;
	line-height: 1;
	text-transform: uppercase;
	height: 34px;
	text-align: center;
}

.nav-li {
	margin-left: 10px;
	/* margin-top: 10px; */
	float: right;
	width: auto;
	margin-right: 1px;
	color: #ffffff;
	text-align: center;
	height: 34px;
	margin-right: 10px;
}

#header {
	width: 70%;
	margin: 0 auto;
	padding: 0;
	display: block;
	position: relative;
}

#logo {
	padding: 1.143em 0 0.437em;
	width: 500px;
	height: 120px;
	float: left;
}

#search-top {
	position: relative;
	margin-top: 0em;
	margin-right: 30px;
	float: right;
}

#search-top input.btn {
	border: none;
	position: absolute;
	width: 16px;
	height: 16px;
	right: 2px;
	top: 4px;
	background: #fff;
}

#navigation {
	width: 100%;
	background-color: #637b01;
	height: 50px;
	float: left;
}

#navigation ul {
	font: normal 14px/1.4em Tahoma, Geneva, Verdana, sans-serif;
	text-transform: uppercase;
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #637b01;
}

#navigation li {
	margin-left: 2%;
	float: left;
	margin-top: 0;
	margin-bottom: 0;
	height: 50px;
	margin-left: 0;
	float: left;
	border-radius: 2px;
}

#navigation li a {
	margin: 0;
	display: inline-block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	display: inline-block;
}

#navigation li a:hover {
	background-color: #ffffff;
	color: #000000;
}

#nav li a {
	margin-top: 0;
	padding: 0;
	margin: 0;
	display: inline-block;
	padding: 2px;
	border-radius: 2px;
	color: white;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	height: 34px;
	margin-top: 10px;
}

#nav li a:hover {
	background-color: #ffffff;
	color: #000000;
	height: auto;
	padding-top: 10px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 10px;
	margin: 0;
	text-align: center;
}

.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5;
	float: left;
}
</style>
</head>

<body>

	<%
		Cookie ck[] = request.getCookies();
		Cookie info = null;
		String name = "";
		if (ck != null) {
			for (Cookie cookie : ck) {
				if (cookie.getName().equals("uname")) {
					info = cookie;
					break;
				}
			}
			if (info != null) {
				name = info.getValue();
			}
		}
	%>
	<div id="top">
		<div class="col-full">
			<ul id="nav">
				<li class="nav-li"><a href="xacnhan.jsp">Kiểm Tra đơn hàng</a></li>
				<li class="nav-li"><a href="thanhtoan.jsp">Thanh toán</a></li>
				<li class="nav-li"><a href="giohang.jsp">Giỏ hàng</a></li>
				<%
					if (name.equals("") || name == null) {
				%>
				<li class="nav-li" style="float: left;"><a href="dangky.jsp">Đăng
						kí</a></li>
				<li class="nav-li" style="float: left;"><a href="dangnhap.jsp">Đăng
						nhập</a></li>
				<%
					} else {
				%>
				<li class="nav-li" style="float: left;"><a href="profile.jsp">Hello,
						<%=name%></a></li>
				<li class="nav-li" style="float: left;"><a href="DangXuat">Đăng
						xuất</a></li>
				<%
					}
				%>
			</ul>
		</div>
	</div>
	<div id="header">
		<a href="index.jsp"> <img alt="" src="images/logo.png"
			class="col-full" id="logo"></a>

		<p align="right" style="color: white; size: 4;">
			<b><br> <br> Hotline</b> <font size="6">0913 908 969</font>
		</p>

		<div id="search-top">

			<form action="<%=request.getContextPath()%>/SearchProduct"
				method="POST">
				<input type="text" onfocus="this.value=''" id="textsearch"
					name="textsearch" value="search for products"> <input
					class="btn" id="btn-search" type="image" src="images/ic-search.png">
			</form>
		</div>

		<div id="navigation" class="col-full">
			<ul>
				<li class="nav-li"><a href="index.jsp">Trang chủ</a></li>
				<li class="nav-li"><a href="introduce.jsp">Giới thiệu</a></li>
				<li class="nav-li"><a href="products.jsp">Sản phẩm</a></li>
				<li class="nav-li"><a href="guide.jsp">H.dẫn mua hàng</a></li>
				<li class="nav-li"><a href="partner.jsp">Cộng tác viên</a></li>
				<li class="nav-li"><a href="catalogue.jsp">Review</a></li>
				<li class="nav-li"><a href="news.jsp">Tin tức</a></li>

				<li class="nav-li"><a href="contact.jsp">Liên hệ</a></li>
			</ul>
		</div>
	</div>
</body>
</html>