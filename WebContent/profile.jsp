<%@page import="com.dieulinh.model.User"%>
<%@page import="com.dieulinh.dao.UserDAO"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.model.GioHang"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thông tin cá nhân</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}
h2 {
	color: #777;
	margin-left: 10px;
	font-weight: normal;
}

th {
	background: white;
}

td {
	padding: 10px;
}

.td {
	border: solid;
	border-width: 0.3px;
	border-color: #e6e6e6;
}

input[type=text]{
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
input[type=password] {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
a {
	color: #8eb001
}

.button {
	float:left;
	margin-left: calc(50% - 35px);
	margin-right: 5px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
#thanhtoan{
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width:70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	vertical-align: baseline;
	
}
.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 ;
	float: left;
}
</style>
</head>
<body>
<%
		Cookie ck[] = request.getCookies();
		Cookie info = null;
		String name = "";
		if (ck != null) {
			for (Cookie cookie : ck) {
				if (cookie.getName().equals("uname")) {
					info = cookie;
					break;
				}
			}
			if (info != null) {
				name = info.getValue();
			}
		}
		UserDAO dao = new UserDAO();
		User user = null;
		if (!name.equals("")) {
			user = dao.existUser(name);
		}
		
	%>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="thanhtoan">
		<%
			if(user!= null){
			
		%>
		<%
			String thongbao = (String) request.getAttribute("thongbao");
			if(thongbao==null) thongbao = "Chỉnh sửa thông tin nếu sai sót!";
			
		%>
		<div class="thongbao"><%=thongbao %>
		</div>
		<form action="CapNhatThongTin" method="POST">
			<div>
				<table cellspacing='0' 
					style="width: 100%; border-collapse: collapse;">

					<!-- Table Header -->
					<thead style='background: #f6f6f6;'>
						<tr>
							<th align="left" style="width: 25%;"><h2>Thông tin cá nhân</h2></th>
							<th align="left" style="width: 25%;">&nbsp;</th>
							<th align="left" style="width: 5%;">&nbsp;</th>
						</tr>
					</thead>

					<!-- Table Body -->
					<tbody>
						<tr>
							<td>
								<p>
									Tên đăng nhập <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="username" value="<%=user.getUsername() %>" readonly>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<p>
									Mật khẩu <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="password"
								name="password" value="<%=user.getPassword() %>">
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<p>
									Nhập lại Mật khẩu <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="password"
								name="repassword" value="<%=user.getPassword() %>">
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<p>
									Họ tên <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="name" value="<%=user.getName() %>">
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>



						</tr>
						<tr>
							<td colspan="2">
								<p>
									Địa chỉ <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="address" value="<%=user.getAddress() %>">
							</td>
							<td>&nbsp;</td>



						</tr>
						<tr>
							<td>
								<p>
									Địa chỉ email <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="email" value="<%=user.getEmail() %>">
							</td>

							<td><p>
									Số Điện Thoại <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="phone" value="<%=user.getPhone() %>"></td>
							<td>&nbsp;</td>


						</tr>
					</tbody>

				</table>

				<input type="submit" class="button"
					style="margin-bottom: 50px; margin-right: 10px;" name="dangky"
					value="Chỉnh sửa">
			</div>
		</form>
		<%
			}else{
				%>
				<div class="thongbao">Bạn phải đăng nhập!
				</div>
				<a href ="dangnhap.jsp"><input type="submit" class="button"
					style="margin-bottom: 50px; margin-right: 10px;" name="dangky"
					value="Đăng nhập"></a>
		<% 	}
		%>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>