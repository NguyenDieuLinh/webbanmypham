package com.dieulinh.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dieulinh.dao.ProductDAO;
import com.dieulinh.model.GioHang;
import com.dieulinh.model.Product;

/**
 * Servlet implementation class UpdateGioHang
 */
@WebServlet("/UpdateGioHang")
public class UpdateGioHang extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateGioHang() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ProductDAO productDao = new ProductDAO();
		Product pro = new Product();
		String productId = "";
		HttpSession session = request.getSession(true);
		PrintWriter out = response.getWriter();

		if (request.getParameter("productId") != null) {
			productId = request.getParameter("productId");
			String co = request.getParameter("value");
			pro = productDao.searchProduct(Integer.parseInt(productId)).get(0);
			GioHang giohang = (GioHang) session.getAttribute("giohang");
			if(giohang==null){
				
			}else{
				if(co.equals("1")){
				giohang.ThemMonhang(pro);
				}
				if(co.equals("-1")){
					giohang.TruMonhang(pro);
				}
				out.print("OK");
			}
			session.setAttribute("lastProduct", pro);
		}
	}

}
