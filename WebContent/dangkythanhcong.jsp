<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đăng ký thành công</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}
h2 {
	color: #777;
	margin-left: 10px;
	font-weight: normal;
}

th {
	background: white;
}

td {
	padding: 10px;
}

.td {
	border: solid;
	border-width: 0.3px;
	border-color: #e6e6e6;
}

input[type=text]{
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
input[type=password] {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
a {
	color: #8eb001
}

.button {
	float:left;
	margin-left: calc(50% - 60px);
	margin-right: 5px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
#thanhtoan{
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width:70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	
	vertical-align: baseline;
	
}
.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
		background: #edfcd5 url(images/ico-tick.png) no-repeat 20px 13px;
	
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 ;
	float: left;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="thanhtoan">
		
		
		<div class="thongbao">Đăng kí thành công<br>
		Hãy kiểm tra hòm thư xác nhận thông tin!
		
		
		</div>
		<a href="index.jsp"> <input type="submit"
				style="margin-right: 0px;" class="button" name="continuebuy"
				value="Tiếp tục mua hàng ⊱"></a>
		</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>