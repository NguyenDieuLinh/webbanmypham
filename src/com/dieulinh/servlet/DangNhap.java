package com.dieulinh.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dieulinh.dao.UserDAO;
import com.dieulinh.model.User;
import com.dieulinh.toolkit.MD5;

/**
 * Servlet implementation class DangNhap
 */
@WebServlet("/DangNhap")
public class DangNhap extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DangNhap() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String thongbao = "";
		if (username == null || username.equals("")) {
			thongbao += "Bạn chưa nhập Tên đăng nhập<br>";
		}
		if (password == null || password.equals("")) {
			thongbao += "Bạn chưa nhập Mật khẩu<br>";
		}

		if (!thongbao.equals("")) {
			request.setAttribute("thongbao", thongbao);
			RequestDispatcher rd = request.getRequestDispatcher("dangnhap.jsp");
			rd.forward(request, response);
			return;
		} else {
			UserDAO dao = new UserDAO();
			User user = dao.existUser(username);
			if (user == null) {
				thongbao = "Tài khoản hoặc Mật khẩu không đúng!";
				request.setAttribute("thongbao", thongbao);
				RequestDispatcher rd = request.getRequestDispatcher("dangnhap.jsp");
				rd.forward(request, response);
				return;
			} else {
				if (user.getPassword().equals(MD5.getHashString(password))) {
					Cookie ck = new Cookie("uname", username);
					ck.setMaxAge(60 * 60);
					response.addCookie(ck);
					response.sendRedirect("dangnhapthanhcong.jsp");
					return;
				} else {
					thongbao = "Tài khoản hoặc Mật khẩu không đúng!";
					request.setAttribute("thongbao", thongbao);
					RequestDispatcher rd = request.getRequestDispatcher("dangnhap.jsp");
					rd.forward(request, response);
					return;
				}
			}

		}

	}
}
