<%@page import="java.util.ArrayList"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.dao.ProductDAO"%>
<%@page import="com.dieulinh.toolkit.Convert"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Chi tiết sản phẩm</title>
<style>
body {
	background-color: #8eb001;
	height: 2000px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#product-detail {
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	width: 48%;
	margin: 0 auto;
	float: left;
	margin-left: 15%;
	background-color: white;
	height: 1200px;
}

#detail {
	margin-left: 30px;
	padding-left: 20px;
}

#product-detail img {
	width: 310px;
}

.btn {
	background-color: #959595;
	width: 24px;
	height: 24px;
	float: left;
	color: #ffffff;
}

#quality {
	float: left;
	height: 20px;
	width: 20px;
	border-style: solid;
	border-color: #959595;
	border-width: 1px;
	border-left-color: #ffffff;
	border-right-color: #ffffff;
	text-align: center;
}

.button {
	float: left;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}

.product {
	margin-top: 20px;
	margin-bottom: 20px;
	width: 25%;
	margin-left: 25px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	padding: 10px;
	float: left;
	margin-right: 5px;
}
</style>
</head>
<body>
	<script type="text/javascript">

	function btnInc(id){
		var dem = document.getElementById(id).value;
		dem++;
		document.getElementById(id).value = dem;
	}
	function btnDec(id){
		var dem = document.getElementById(id).value;

		if(dem>1){
			dem--;
			document.getElementById(id).value = dem;
		}
	}
	
</script>
	<jsp:include page="header.jsp"></jsp:include>
	<%
		ProductDAO productDao = new ProductDAO();
		Product pro = new Product();
		String productId = "";
		if (request.getParameter("productId") != null) {
			productId = request.getParameter("productId");
			pro = productDao.searchProduct(Integer.parseInt(productId)).get(0);
		}
	%>
	<div id="product-detail">
		<form action='ThemHangVaoGio?productId=<%=pro.getId()%>' method='POST'>
			<table style='width: 100%' cellspacing='0'>
				<tr>
					<td style='width: 50%; box-shadow: 0px 1px 0px #888888;'><img
						style='margin-left: 10px;' alt="" src="images/<%=pro.getImage()%>">
					</td>
					<td style="box-shadow: 0px 1px 0px #888888;">
						<div id="detail">
							<h3><%=pro.getName()%></h3>
							<p style="margin-right: 40px;"><%=pro.getDescription()%></p>
							<table>

								<tr>
									<td>
										<h4>Hãng sản xuất</h4>
									</td>
									<td style="margin-left: 10px; padding-left: 20px;"><%=pro.getHangsx()%>
									</td>
								</tr>
								<tr>
									<td>
										<h4>Giá bán:</h4>
									</td>

									<td style="margin-left: 10px; padding-left: 20px;"><%=pro.getPrice()%>
									</td>
								</tr>

								<tr>
									<td><h4>Số lượng</h4></td>
									<td class="product-quantity">
										<div style="float: left">
											<input type="button" value="-"
												onclick="btnDec(<%=pro.getId()%>)"> <input
												type="number" name='soluong' id="<%=pro.getId()%>" step="1"
												min="1" max="50" value="<%=pro.getSoluong()%>"> <input
												type="button" value="+" onclick="btnInc(<%=pro.getId()%>)">
										</div>
									</td>
								</tr>
								<tr>
									<td><input type='submit' class="button"
										value="Thêm vào giỏ"></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</form>
		<div id="rela-products">
			<h3
				style="text-transform: uppercase; color: #777777; margin-left: 5%;">Sản
				phẩm liên quan</h3>

			<%
				int categoryId = pro.getCatagory();
				ArrayList<Product> arr = productDao.getProductByCategory(categoryId);
				if (arr.isEmpty())
					arr = new ArrayList<>();
				for (Product p : arr) {
			%>
			<div class="product" style="width: 25%;">
				<a href="detail.jsp?productId=<%=p.getId()%>"> <img alt=""
					src="images/<%=p.getImage()%>" height="200px" width="200px"
					style="margin-left: 5%; float: left; width: 90%; height: 90%">
				</a>
				<h4 style="color: #8eb001; text-align: center;"><%=p.getName()%></h4>
				<h3 style="color: red;" align="center"><%=Convert.ToString(p.getPrice())%>
					₫
				</h3>
				<form action="ThemHangVaoGio?productId=<%=p.getId()%>" method='POST'>
					<button class="button" style="margin-left: 25%">Mua hàng</button>
				</form>
			</div>
			<%
				}
			%>
		</div>
	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>