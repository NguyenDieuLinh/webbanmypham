<%@page import="com.dieulinh.toolkit.Convert"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.model.GioHang"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Giỏ hàng</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

table {
	width: calc(100% - 20px);
	margin: 10px;
	border-collapse: collapse;
}

tr, th {
	border-style: solid;
	border-width: 0.3px;
	border-color: #d4ebaf;
}

a {
	color: #8eb001
}

th {
	padding: 5px;
}

td {
	border-style: solid;
	border-width: 0.3px;
	border-color: #d4ebaf;
	padding-left: 5px;
}

.button {
	float: right;
	margin-left: 5px;
	margin-right: 5px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 url(images/ico-tick.png) no-repeat 20px 13px;
	float: left;
}

h2 {
	color: #777;
	margin-left: 10px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}

#giohang {
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width: 70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	vertical-align: baseline;
	background-color: white;
	color: #8eb001;
}
</style>
</head>
<body>
	<script type="text/javascript">

	function btnInc(id){
		var dem = document.getElementById(id).value;
		dem++;
		document.getElementById(id).value = dem;
		update(id,1);
	}
	function btnDec(id){
		var dem = document.getElementById(id).value;
		if(dem>1){
		dem--;
		document.getElementById(id).value = dem;
		update(id,-1);
		}
	}
	function update(id, co) {
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (xhttp.readyState == 4 && xhttp.status == 200) {
		    }
		  };
		  xhttp.open("GET", "UpdateGioHang?productId="+id+"&value="+co, true);
		  xhttp.send();
	}
</script>
	<%
		GioHang giohang = (GioHang) request.getSession().getAttribute("giohang");
		if (giohang == null) {
			giohang = new GioHang();
		}
		String thongbao = (String) request.getSession().getAttribute("thongbao");
		if (thongbao == null) {
			thongbao = "Bạn chưa chọn thêm món hàng nào!";
		} else {
			request.getSession().removeAttribute("thongbao");
		}
	%>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>

	<div id="giohang">
		<%
			if (!thongbao.equals("")) {
		%>
		<div class="thongbao"><%=thongbao%>
			<a href="index.jsp"> <input type="submit"
				style="margin-right: 0px;" class="button" name="continuebuy"
				value="Tiếp tục mua hàng ⊱"></a>
		</div>
		<%
			} else {
		%>
		<div class="thongbao">
			Giỏ hàng chưa có sản phẩm nào <a href="index.jsp"> <input
				type="submit" style="margin-right: 0px;" class="button"
				name="continuebuy" value="Quay lại mua hàng ⊱"></a>
		</div>
		<%
			}
		%>

		<div>
			<table cellspacing='0'>

				<!-- Table Header -->
				<thead style='background: #f6f6f6;'>
					<tr>
						<th align="left">&nbsp;</th>
						<th align="left">&nbsp;</th>
						<th align="left" style="font-weight: normal;">Tên sản phẩm</th>
						<th align="left" style="font-weight: normal;">Đơn giá</th>
						<th align="left" style="font-weight: normal;">Số lượng</th>
						<th align="left" style="font-weight: normal;">Tổng cộng</th>
					</tr>
				</thead>

				<!-- Table Body -->
				<tbody>
					<%
						ArrayList<Product> listHang = giohang.getGiohang();
						for (int i = 0; i < listHang.size(); i++) {
							Product product = listHang.get(i);
					%>
					<tr>
						<td class="product-remove"><a
							href="XoaHangKhoiGio?productId=<%=product.getId()%>"
							class="remove" title="Xóa">×</a></td>
						<td class="product-thumbnail"><a href="#"> <img
								width="50px" height="50px" src="images/<%=product.getImage()%>"
								alt="1">
						</a></td>
						<td class="product-name"><a
							href="detail.jsp?productId=<%=product.getId()%>"><%=product.getName()%></a></td>
						<td class="product-price"><span class="amount"><%=Convert.ToString(product.getPrice())%></span></td>
						<td class="product-quantity">
							<div style="float: left">
								<input type="button" value="-"
									onclick="btnDec(<%=product.getId()%>)"> <input
									type="number" id="<%=product.getId()%>" step="1" min="1"
									max="50" value="<%=product.getSoluong()%>"> <input
									type="button" value="+" onclick="btnInc(<%=product.getId()%>)">
							</div>
						</td>
						<td class="product-subtotal"><span class="amount"
							id="a<%=product.getId()%>"><%=Convert.ToString(product.getTongTien())%></span></td>

					</tr>
					<%
						}
					%>
					<!-- Table Row -->



					<tr>
						<td colspan="6" class="actions"><a href="thanhtoan.jsp"><input
								type="submit" class="button"
								style="margin-top: 10px; margin-bottom: 10px;" name="proceed"
								value="Thanh toán"> </a> <a href="giohang.jsp"><input
								style="margin-top: 10px; margin-bottom: 10px;" type="submit"
								class="button" name="update_cart" value="Cập nhật giỏ hàng"></a></td>
					</tr>

				</tbody>
				<!-- Table Body -->

			</table>
		</div>
		<div>
			<h2>Thông tin thanh toán</h2>
			<table cellspacing='0'>
				<tbody>

					<tr>
						<td style="background: #f6f6f6; padding-left: 5px;" width="20%"><p
								style="font-weight: bold;">Thành tiền</p></td>
						<td><%=Convert.ToString(giohang.tongtien())%> VNĐ</td>
					</tr>
					<tr>
						<td style="background: #f6f6f6; padding-left: 5px;"><p>Chi
								phí vận chuyển</p></td>
						<td><p style="color: #777;">30.000 VNĐ</p></td>
					</tr>
					<tr>
						<td style="background: #f6f6f6; padding-left: 5px;"><p
								style="font-weight: bold;">Tổng tiền</p></td>
						<td><%=Convert.ToString(giohang.tongtien() + 30000)%> VNĐ</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>