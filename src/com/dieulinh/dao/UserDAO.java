package com.dieulinh.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.dieulinh.model.Product;
import com.dieulinh.model.User;
import com.mysql.jdbc.PreparedStatement;

public class UserDAO {
	private Connection con = ConfigJDBC.initConnection();
	
	public void addUser(User a){
		String query = "insert into user(username,password,name,address,email,phone) "
				+ " values (?,?,?,?,?,?);";
		PreparedStatement ps;
		try {
			ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, a.getUsername());
			ps.setString(2, a.getPassword());
			ps.setString(3,a.getName());
			ps.setString(4, a.getAddress());
			ps.setString(5, a.getEmail());
			ps.setString(6, a.getPhone());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	public User existUser(String username) {
		User a = null;
		String query = "select * from user where username='"+username+"';";
		PreparedStatement st;
		try {
			st = (PreparedStatement) con.prepareStatement(query);
			ResultSet rs = st.executeQuery();
			
			while (rs.next()) {
				a = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
				return a;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}

	
	
	
	
	
	
	
	
	
	
	public void editUser(User a){
		String query = "UPDATE user SET password=?, name=?, address=?, email = ?, phone = ? WHERE id=?;";
		PreparedStatement ps;
		try {
			ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, a.getPassword());
			ps.setString(2,a.getName());
			ps.setString(3, a.getAddress());
			ps.setString(4, a.getEmail());
			ps.setString(5, a.getPhone());
			ps.setInt(6, a.getId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
