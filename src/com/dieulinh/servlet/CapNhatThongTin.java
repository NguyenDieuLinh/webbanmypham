package com.dieulinh.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dieulinh.dao.UserDAO;
import com.dieulinh.model.User;
import com.dieulinh.toolkit.MD5;

/**
 * Servlet implementation class CapNhatThongTin
 */
@WebServlet("/CapNhatThongTin")
public class CapNhatThongTin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CapNhatThongTin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String thongbao = "";
		if(username==null || username.equals("")){
			thongbao += "Bạn chưa nhập Tên đăng nhập<br>";
		}
		if(password==null || password.equals("")){
			thongbao += "Bạn chưa nhập Mật khẩu<br>";
		}
		if(repassword==null || repassword.equals("")){
			thongbao += "Bạn chưa nhập Mật khẩu<br>";
		}
		if(name==null || name.equals("")){
			thongbao += "Bạn chưa nhập Họ tên<br>";
		}
		if(address==null || address.equals("")){
			thongbao += "Bạn chưa nhập Địa chỉ<br>";
		}
		if(email==null || email.equals("")){
			thongbao += "Bạn chưa nhập Email<br>";
		}
		if(phone==null || phone.equals("")){
			thongbao += "Bạn chưa nhập Số điện thoại<br>";
		}
		if(!repassword.equals(password)){
			thongbao += "Mật khẩu xác nhận không giống nhau<br>";
		}
		if(!thongbao.equals("")){
			request.setAttribute("thongbao", thongbao);
			RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
			rd.forward(request, response);
			return;
		}else{
			UserDAO dao = new UserDAO();
			User b = dao.existUser(username);
			if(b==null){
			
				thongbao="Có lỗi!";
				request.setAttribute("thongbao", thongbao);
				RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
				rd.forward(request, response);
				return;
			}else{
				b.setPassword(MD5.getHashString(password));
				b.setName(name);
				b.setAddress(address);
				b.setPhone(phone);
				b.setEmail(email);
				dao.editUser(b);
				
				response.sendRedirect("chinhsuathanhcong.jsp");
				return;
				
			}
		}
	}

}
