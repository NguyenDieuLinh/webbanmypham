<%@page import="com.dieulinh.model.User"%>
<%@page import="com.dieulinh.dao.UserDAO"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.model.GioHang"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thanh toán</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

h2 {
	color: #777;
	margin-left: 10px;
	font-weight: normal;
}

th {
	background: white;
}

td {
	padding: 10px;
}

.td {
	border: solid;
	border-width: 0.3px;
	border-color: #e6e6e6;
}

input[type=text] {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}

a {
	color: #8eb001
}

.button {
	float: right;
	margin-left: 5px;
	margin-right: 5px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}

#thanhtoan {
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width: 70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	vertical-align: baseline;
}

.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 url(images/ico-tick.png) no-repeat 20px 13px;
	float: left;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>

	<%
		Cookie ck[] = request.getCookies();
		Cookie info = null;
		String name = "";
		if (ck != null) {
			for (Cookie cookie : ck) {
				if (cookie.getName().equals("uname")) {
					info = cookie;
					break;
				}
			}
			if (info != null) {
				name = info.getValue();
			}
		}
		UserDAO dao = new UserDAO();
		User user = null;
		if (!name.equals("")) {
			user = dao.existUser(name);
		}
		if (user == null)
			user = new User();
	%>
	<div id="thanhtoan">
		<h4 style="color: #777; margin-left: 10px; margin-top: 70px;">Thanh
			toán</h4>
		<%
			GioHang giohang = (GioHang) request.getSession().getAttribute("giohang");
			if (giohang == null) {
				giohang = new GioHang();
			}
		%>
		<div class="thongbao">Hãy điền đầy đủ thông tin chính xác nhất!
		</div>
		<form action="CapNhatHoaDon" method="POST">
			<div>
				<table cellspacing='0'
					style="width: 100%; border-collapse: collapse;">

					<!-- Table Header -->
					<thead style='background: #f6f6f6;'>
						<tr>
							<th align="left" style="width: 25%;"><h2>Thông tin
									thanh toán</h2></th>
							<th align="left" style="width: 25%;">&nbsp;</th>
							<th align="left" style="width: 5%;">&nbsp;</th>
							<th align="left" style="width: 25%;"><h2>Địa chỉ nhận
									hàng</h2></th>
							<th align="left">&nbsp;</th>

						</tr>
					</thead>

					<!-- Table Body -->
					<tbody>

						<tr>
							<td>
								<p>
									Họ tên <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="namefrom" value="<%=user.getName()%>">
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><p>
									Họ tên <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="nameto"></td>
							<td>&nbsp;</td>


						</tr>
						<tr>
							<td colspan="2">
								<p>
									Địa chỉ <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="addressfrom" value="<%=user.getAddress()%>">
							</td>
							<td>&nbsp;</td>
							<td colspan="2">
								<p>
									Địa chỉ <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 20px);" type="text"
								name="addressto">
							</td>


						</tr>
						<tr>
							<td>
								<p>
									Địa chỉ email <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="email" value="<%=user.getEmail()%>">
							</td>

							<td><p>
									Số Điện Thoại <span style="color: red">*</span>
								</p> <input style="width: calc(100% - 10px);" type="text"
								name="phonenumber" value="<%=user.getPhone()%>"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>


						</tr>
						<!-- Table Row -->

					</tbody>
					<!-- Table Body -->

				</table>
				<h2>Đơn hàng của bạn</h2>
				<table cellspacing='0'
					style="width: calc(100% - 20px); border-collapse: collapse; margin-left: 10px;">
					<tbody>

						<tr>
							<td class='td' style="background: #f6f6f6; padding-left: 5px;"
								width="50%"><p style="font-weight: normal;">Tên sản
									phẩm</p></td>
							<td class='td' style="padding-left: 5px;"><p
									style="font-weight: normal;">Tổng cộng</p></td>
						</tr>
						<%
							for (int i = 0; i < giohang.getGiohang().size(); i++) {
								Product product = giohang.getGiohang().get(i);
						%>
						<tr>

							<td class='td' style="padding-left: 5px;" width="20%"><a
								href="detail.jsp?productId=<%=product.getId()%>">
									<p style="font-weight: normal;"><%=product.getName()%>
							</a> x <%=product.getSoluong()%></p></td>
							<td class='td' style="padding-left: 5px;"><p
									style="font-weight: normal;"><%=product.getPrice()%>
									đ
								</p></td>
						</tr>
						<%
							}
						%>
						<tr>
							<td class='td' style="background: #f6f6f6; padding-left: 5px;"><p>Chi
									phí vận chuyển</p></td>
							<td class='td'><p>30.000 đ</p></td>
						</tr>
						<tr>
							<td class='td' style="background: #f6f6f6; padding-left: 5px;"><p
									style="font-weight: bold;">Tổng tiền</p></td>
							<td class='td'><%=giohang.tongtien()%> đ</td>
						</tr>
					</tbody>
				</table>
				<p style='color: #777; margin-top: 10px; margin-left: 10px;'>Thanh
					toán bằng chuyển khoản ngân hàng</p>
				<p
					style='width: calc(100% - 30px); color: #777; margin-top: 10px; margin-left: 10px; background: #f6f6f6; border-radius: 2px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; border: 1px solid #e6e6e6;'>
					Ngân hàng Vietcombank – Chi nhánh Phú Thọ<br> Chủ tài khoản:
					Nguyễn Diệu Linh <br> Số tài khoản: 0421 000 443 693 <br>
					<br> Ngân hàng Sacombank – PGD PTIT Hoàng Gia <br> Chủ tài
					khoản: Nguyễn Diệu Linh <br> Số tài khoản: 0600 8278 8797 <br>
					<br> Ngân hàng Agribank – CN PTIT Hà Nội <br> Chủ tài
					khoản: Nguyễn Hoàng Nam <br> Số tài khoản: 1969 9696 123 699
				</p>
				<input type="submit" class="button"
					style="margin-bottom: 50px; margin-right: 10px;" name="proceed"
					value="Đặt hàng">
			</div>
		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>