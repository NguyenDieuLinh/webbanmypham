package com.dieulinh.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dieulinh.dao.DonHangDAO;
import com.dieulinh.model.DonHang;

/**
 * Servlet implementation class XacNhanDonHang
 */
@WebServlet("/XacNhanDonHang")
public class XacNhanDonHang extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public XacNhanDonHang() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		DonHang donhang = (DonHang) session.getAttribute("donhang");
		if(donhang!=null){
		DonHangDAO dao = new DonHangDAO();
		dao.addDonHang(donhang);
		response.sendRedirect("muathanhcong.jsp");
		} else {
			response.sendRedirect("thanhtoan.jsp");
		}
	}

}
