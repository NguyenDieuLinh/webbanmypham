package com.dieulinh.model;

import java.util.Calendar;
import java.util.Date;

public class DonHang {
	private int id;



	private User user;
	private String tennguoinhan;
	private String diachinhan;
	private Date ngaydat;
	private GioHang giohang;
	public DonHang(User user, String tennguoinhan, String diachinhan, Date ngaydat, GioHang giohang) {
		super();
		this.user = user;
		this.tennguoinhan = tennguoinhan;
		this.diachinhan = diachinhan;
		this.ngaydat = ngaydat;
		this.giohang = giohang;
	}


	public DonHang() {
		user = new User();
		Calendar cal = Calendar.getInstance();
		ngaydat = cal.getTime();
		giohang = new GioHang();
	}

	
	public DonHang(User user, String tennguoinhan, String diachinhan) {
		super();
		this.user = user;
		this.tennguoinhan = tennguoinhan;
		this.diachinhan = diachinhan;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTennguoinhan() {
		return tennguoinhan;
	}

	public void setTennguoinhan(String tennguoinhan) {
		this.tennguoinhan = tennguoinhan;
	}

	public String getDiachinhan() {
		return diachinhan;
	}

	public void setDiachinhan(String diachinhan) {
		this.diachinhan = diachinhan;
	}


	public Date getNgaydat() {
		return ngaydat;
	}


	public void setNgaydat(Date ngaydat) {
		this.ngaydat = ngaydat;
	}


	public GioHang getGiohang() {
		return giohang;
	}


	public void setGiohang(GioHang giohang) {
		this.giohang = giohang;
	}
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
}
