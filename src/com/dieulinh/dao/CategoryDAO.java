package com.dieulinh.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.dieulinh.model.Category;

public class CategoryDAO {
	private Connection con = ConfigJDBC.initConnection();

	public CategoryDAO() {
	}

	public ArrayList<Category> getListCategory() {
		ArrayList<Category> list = new ArrayList<>();
		String sql = "SELECT * FROM category";

		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Category cate = new Category(rs.getInt(1), rs.getString(2), rs.getString(3));
				list.add(cate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
}
