<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#contact {
	color: #777777;
	background-color: white;
	width: 43%;
	height: 1200px;
	float: left;
	margin: 0 auto;
	margin-left: 15%;
	padding-left: 5%;
}

#contact td input {
	width: 120%;
	height: 20px;
	margin-top: 10px;
	margin-left: 20px;
}

#contact button {
	float: left;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
	margin-top: 10px;
	margin-left: 20px;
}

#contact button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="contact">
		<h3>Liên hệ</h3>
		<h4>Địa chỉ: 40B - An Hòa - Hà Đông - Hà Nội</h4>
		<h4>Điện thoại: 0913 908 969 - 01669353568 -Email:
			linhnd1208@gmail.com</h4>
		<table>
			<tr>
				<td>Tên</td>
				<td><input type="text"></td>
			</tr>

			<tr>
				<td>Email</td>
				<td><input type="text"></td>
			</tr>

			<tr>
				<td>Thông điệp</td>
				<td><input type="text" style="width: 150%; height: 120px;"></td>
			</tr>

			<tr>
				<td></td>
				<td><button>Gửi thông tin</button></td>
			</tr>
		</table>
	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>