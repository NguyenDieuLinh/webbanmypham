package com.dieulinh.model;

public class Product {
	private int id;
	private String name;
	private long price;
	private String description;
	private int category;
	private String image;
	private String khoiLuong;
	private String hangsx;
	private int soluong = 1;
	public Product(int id, String name, long price, String description, int catagory, String image, String khoiLuong,
			String hangsx, int soluong) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.category = catagory;
		this.image = image;
		this.khoiLuong = khoiLuong;
		this.hangsx = hangsx;
		this.soluong = soluong;
	}

	public int getSoluong() {
		return soluong;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getKhoiLuong() {
		return khoiLuong;
	}

	public void setKhoiLuong(String khoiLuong) {
		this.khoiLuong = khoiLuong;
	}

	public String getHangsx() {
		return hangsx;
	}

	public void setHangsx(String hangsx) {
		this.hangsx = hangsx;
	}

	public Product() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCatagory() {
		return category;
	}

	public void setCatagory(int catagory) {
		this.category = catagory;
	}
	public long getTongTien(){
		return this.getPrice() * this.getSoluong();
	}
}
