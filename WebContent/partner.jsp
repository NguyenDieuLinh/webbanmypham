<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#partner {
	width: 46%;
	float: left;
	margin-left: 15%;
	background-color: #ffffff;
	height: 1200px;
	padding-left: 2%;
}

#partner h4 {
	text-transform: uppercase;
	color: #777777;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="partner">
		<h3 style="text-transform: uppercase; color: #777777">Cộng tác
			viên</h3>
		<h4>Tuyển cộng tác viên bán hàng mỹ phẩm</h4>

		<img alt="" src="images/cong-tac-vien.jpg"
			style="width: 70%; height: auto; margin-left: 15%;" align="middle">

		<h4>Vị trí cộng tác viên</h4>
		<p>Không cần bỏ vốn nên không có rủi ro khi làm việc – Không mất
			thời gian – Không hạn chế độ tuổi, xa gần ,giới tính, ngành nghề,
			tuyển toàn quốc – Không giới hạn vùng miền .Ưu tiên cho các cộng tác
			viên có đam mê yêu thích kinh doanh mỹ phẩm dưỡng da.</p>

		<h4>Yêu cầu chung</h4>
		<p>Tất cả mọi người có khả năng bán hàng tốt, hiểu viết về mỹ
			phẩm. Khi có khách mua hàng, bạn nhắn tin hoặc check xem hàng còn hay
			hết .Ctv có thể đến shop để nhận hàng và thanh toán 100% tự đem giao
			cho khách . Trường Hợp Ctv Không Thể Tự giao hàng .Ctv không cần tới
			shop lấy hàng giao cho khách ,khi có khách mua, chuyển thông tin cho
			shop để shop giao hàng Tất cả đơn hàng được theo dõi chặt chẽ sẽ gửi
			thông báo ngay cho ctv, đảm bảo không thiếu sót nên ctv hoàn toàn yên
			tâm làm việc.</p>

		<h4>* Lưu ý:</h4>
		<p>1. Các bạn có thể thu lại tiền ship với khách (Phí hợp lí từ 10
			– 25k tùy khu vực bạn giao trong nội thành HCM, 30 – 55k đối với
			khách ở tỉnh hàng nặng cồng kềnh).</p>
		<p>2. Shop chỉ giữ hàng cho CTV đến lấy hàng trong vòng 24h, CTV
			kêu giữ hàng hoặc nhờ shop giao khách nhưng hủy quá 3 lần shop sẽ hủy
			bỏ chức vụ CTV của bạn đó.</p>
	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>