<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Slider</title>


<script type="text/javascript">
	var img1 = new Image();
	img1.src = "images/slider-1.jpg";

	var img2 = new Image();
	img2.src = "images/slider-2.jpg";

	var img3 = new Image();
	img3.src = "images/slider-3.jpg";

	var img4 = new Image();
	img4.src = "images/slider-4.jpg";

	var img5 = new Image();
	img5.src = "images/slider-5.jpg";
</script>
</head>
<body>
	<img alt="" src="slider-1.jpg" name="slide" width="70%"
		style="margin-left: 15%;" height="400px;">
	<script type="text/javascript">
		var step = 1;
		function slideit() {
			document.images.slide.src = eval("img" + step + ".src");
			if (step < 5) {
				step++;
			} else {
				step = 1;
			}
			setTimeout("slideit()", 1500);
		}
		slideit();
	</script>
</body>
</html>