<%@page import="com.dieulinh.toolkit.Convert"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="com.dieulinh.model.GioHang"%>
<%@page import="com.dieulinh.model.DonHang"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Xác nhận đơn hàng</title>
<style>
th {
	background: white;
}

td {
	padding: 10px;
}

.td {
	border: solid;
	border-width: 0.3px;
	border-color: #e6e6e6;
}

a {
	color: #8eb001;
}
p{
	color: #777;
}
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}
#xacnhan{
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width:70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	vertical-align: baseline;
}
.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 url(images/ico-tick.png) no-repeat 20px 13px;
	float: left;
}
.button {
	
	margin-left: calc(50% - 30px);
	margin-right: 5px;
	margin-bottom: 100px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
</style>
</head>
<body>
	<%
		GioHang giohang = (GioHang) request.getSession().getAttribute("giohang");
		if (giohang == null) {
			giohang = new GioHang();
		}
		DonHang donhang = (DonHang) request.getSession().getAttribute("donhang");
		if(donhang==null) donhang = new DonHang();
		Calendar cal = Calendar.getInstance();
		cal.setTime(donhang.getNgaydat());
		
	%>
	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	
	<div id="xacnhan">
	<div class="thongbao">
			Hãy kiểm tra lại thông tin và nhấn 'Xác nhận'! 
	</div>
	
	<p style='margin-left: 10px; font-family: Tahoma; color: #777;'>
		Đơn hàng của bạn đã được cập nhật. Chúng tôi sẽ xử lý đơn hàng của bạn
		trong thời gian nhanh nhất.<br> <br>##############################<br>
		Mã đơn hàng: <span style='font-weight: bold;'>#<%=donhang.getId() %></span><br>
		Ngày đặt hàng: <span style='font-weight: bold;'><%=cal.get(Calendar.DAY_OF_MONTH) %>/<%=cal.get(Calendar.MONTH) %>/<%=cal.get(Calendar.YEAR)%></span><br>
		Tổng tiền: <span style='font-weight: bold;'><%=Convert.ToString(donhang.getGiohang().tongtien()) %> đ</span><br>
		Phương thức thanh toán: <span style='font-weight: bold;'>Thanh
			toán bằng chuyển khoản ngân hàng</span> <br>##############################<br>
	</p>
	<p
		style='color: #777; margin-top: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
		Ngân hàng Vietcombank – Chi nhánh Phú Thọ<br> Chủ tài khoản: Lê
		Thị Minh Phương <br> Số tài khoản: 0421 000 443 693 <br> <br>
		Ngân hàng Sacombank – PGD Bình Trị Đông <br> Chủ tài khoản: Lê
		Thị Minh Phương <br> Số tài khoản: 0600 8278 8797 <br> <br>
		Ngân hàng Agribank – CN KCN Tân Tạo <br> Chủ tài khoản: Lê Thị
		Minh Phương <br> Số tài khoản: 1903 206 123 600
	</p>
	<h2 style='margin-left: 10px; color: #777;'>
		Our Details<br> <br> Chi tiết đơn hàng
	</h2>
	<table cellspacing='0'
		style="width: calc(100% - 20px); border-collapse: collapse; margin-left: 10px;">
		<tbody>

			<tr>
				<td class='td' style="background: #f6f6f6; padding-left: 5px;"
					width="50%"><p style="font-weight: normal;">Tên sản phẩm</p></td>
				<td class='td' style="padding-left: 5px;"><p
						style="font-weight: normal;">Tổng cộng</p></td>
			</tr>
			<%
				for (int i = 0; i < giohang.getGiohang().size(); i++) {
					Product product = giohang.getGiohang().get(i);
			%>
			<tr>

				<td class='td' style="padding-left: 5px;" ><p style="font-weight: normal;"><a
					href="detail.jsp?productId=<%=product.getId()%>">
						<%=product.getName()%>
				</a> x <%=product.getSoluong()%></p></td>
				<td class='td' style="padding-left: 5px;"><p
						style="font-weight: normal;"><%=Convert.ToString(product.getPrice())%>
						đ
					</p></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td class='td' style="background: #f6f6f6; padding-left: 5px;"><p>Chi
						phí vận chuyển</p></td>
				<td class='td'><p style="color: #777;">30.000 VNĐ</p></td>
			</tr>
			<tr>
				<td class='td' style="background: #f6f6f6; padding-left: 5px;"><p
						style="font-weight: bold;">Tổng tiền</p></td>
				<td class='td'><%=Convert.ToString(giohang.tongtien() + 30000)%> đ</td>
			</tr>
		</tbody>
	</table>

	<h2 style='margin-left: 10px; color: #777;'>
		Thông tin đặt hàng<br>

	</h2>
	<p
		style='color: #777; padding-left: 10px; padding-bottom: 10px; line-height: 200%;'>
		Email: <%=donhang.getUser().getEmail() %> <br> Số điện thoại: <%=donhang.getUser().getPhone()%>
	</p>
	<table cellspacing='0' 
		style="width: 100% ; border-collapse: collapse; margin-left: 10px;" >
		<tr>
			<th style='width:50%;'><h2 style='float:left; color: #777;'>Địa chỉ thanh toán</h2></th>
			<th ><h2 style=' color: #777; float:left;'>Địa chỉ nhận hàng</h2></th>
		</tr>
		<tr>
			<td ><p><%=donhang.getUser().getName() %></p></td>
			<td ><p><%=donhang.getTennguoinhan() %></p></td>
		</tr>
		<tr>
			<td ><p><%=donhang.getUser().getAddress() %></p></td>
			<td ><p><%=donhang.getDiachinhan() %></p></td>
		</tr>
	</table>
	<form action="XacNhanDonHang" method='POST'>
		<input type='submit' class='button' value='Xác nhận'>
	</form>
	</div>
	
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>