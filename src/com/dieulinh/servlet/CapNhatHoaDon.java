package com.dieulinh.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dieulinh.model.DonHang;
import com.dieulinh.model.GioHang;
import com.dieulinh.model.User;

/**
 * Servlet implementation class CapNhatHoaDon
 */
@WebServlet("/CapNhatHoaDon")
public class CapNhatHoaDon extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CapNhatHoaDon() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
doPost(request, response);	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		User a = new User();
		a.setName(request.getParameter("namefrom")==null?"":request.getParameter("namefrom"));
		a.setAddress(request.getParameter("addressfrom")==null?"":request.getParameter("addressfrom"));
		a.setPhone(request.getParameter("phonenumber")==null?"":request.getParameter("phonenumber"));
		a.setEmail(request.getParameter("email")==null?"":request.getParameter("email"));
		
		DonHang donhang = new DonHang();
		donhang.setUser(a);
		donhang.setDiachinhan(request.getParameter("addressto")==null?"":request.getParameter("addressto"));
		donhang.setTennguoinhan(request.getParameter("nameto")==null?"":request.getParameter("nameto"));
		
		HttpSession session = request.getSession(true);

		
		GioHang giohang = (GioHang) session.getAttribute("giohang");
		if(giohang == null) giohang = new GioHang();
		donhang.setGiohang(giohang);
		
		session.setAttribute("donhang", donhang);
	
		response.sendRedirect("xacnhan.jsp");
		
	}

}
