<%@page import="com.dieulinh.toolkit.Convert"%>
<%@page import="com.dieulinh.model.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.dieulinh.dao.ProductDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Products</title>
<style>
body {
	background-color: #8eb001;
	height: 1200px;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}

#product {
	color: #777777;
	background-color: white;
	width: 48%;
	height: 1200px;
	float: left;
	margin: 0 auto;
	margin-left: 15%;
}

._product {
	margin-top: 20px;
	margin-bottom: 20px;
	width: 25%;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	padding: 10px;
	float: left;
	margin-right: 2%;
	margin-left: 3%;
}

#submit {
	float: left;
	margin-left: 25%;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
	width: 60px;
}

#submit:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}

.button {
	float: left;
	margin-left: 25%;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
</style>
</head>
<body>
	<%
		ProductDAO proDao = new ProductDAO();
		ArrayList<Product> arr = new ArrayList<>();

		arr = proDao.getListProducts();
		if (request.getAttribute("sortArr") instanceof ArrayList) {
			arr = (ArrayList<Product>) request.getAttribute("sortArr");
		}
	%>

	<jsp:include page="header.jsp"></jsp:include>
	<jsp:include page="slider.jsp"></jsp:include>
	<div id="product">
		<form action="Sort" method="get">
			<table style="width: 100%">
				<tr>
					<td style="padding-left: 2%"><h4
							style="color: #777777; float: left;">
							Có
							<%=arr.size()%>
							Sản phẩm
						</h4></td>
					<td style="padding-right: 2%"><select
						style="float: right; margin-left: 10px;"
						onChange="combo(this, 'theinput')" name="combo">
							<option>Mặc định</option>
							<option>Mới nhất</option>
							<option>Giá từ thấp tới cao</option>
							<option>Giá từ cao xuống thấp</option>
					</select></td>

					<td><input type="submit" value="Sort" id="submit"></td>
				</tr>
			</table>
		</form>
		<%
			for (Product p : arr) {
		%>
		<div class="_product">
			<a href="detail.jsp?productId=<%=p.getId()%>"> <img alt=""
				src="images/<%=p.getImage()%>" width="90%" height="90%"
				style="float: left;">
			</a>
			<h4 style="color: #8eb001; text-align: center;"><%=p.getName()%></h4>
			<h3 style="color: red;" align="center"><%=Convert.ToString(p.getPrice())%>
				₫
			</h3>
			<form action="ThemHangVaoGio?productId=<%=p.getId()%>" method='POST'>
				<button class="button">Mua hàng</button>
			</form>
		</div>
		<%
			}
		%>

	</div>
	<jsp:include page="sidebar.jsp"></jsp:include>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>