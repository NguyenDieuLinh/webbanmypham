package com.dieulinh.model;

public class Category {
	private int idCategory;
	private String mainCategoryName;
	private String childCategoryName;

	public Category(int idCategory, String mainCategoryName, String childCategoryName) {
		super();
		this.idCategory = idCategory;
		this.mainCategoryName = mainCategoryName;
		this.childCategoryName = childCategoryName;
	}

	public Category() {
		super();
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	public String getMainCategoryName() {
		return mainCategoryName;
	}

	public void setMainCategoryName(String mainCategoryName) {
		this.mainCategoryName = mainCategoryName;
	}

	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}

}
