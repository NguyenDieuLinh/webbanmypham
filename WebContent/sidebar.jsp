<%@page import="com.dieulinh.model.Category"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.dieulinh.dao.CategoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>category</title>
<style>
#sidebar {
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	float: right;
	overflow: hidden;
	width: 22%;
	border: 0;
	outline: 0;
	vertical-align: baseline;
	background-color: white;
	float: right;
	color: #8eb001;
	height: 1200px;
}

#sidebar li a {
	padding-bottom: 5px; margin : 0;
	display: inline-block;
	color: #8eb001;
	text-decoration: none;
	margin: 0;
}
</style>
</head>
<body>
	<div id="sidebar">
		<p
			style="text-transform: uppercase; color: #777777; font-size: 16px; margin-left: 5%">Danh
			mục sản phẩm</p>
		<ul>
			<%
				CategoryDAO cateDao = new CategoryDAO();
				ArrayList<Category> arr = cateDao.getListCategory();
				for (int i = 0; i < arr.size(); i++) {
					String mainCategoryName = "";
					String childCategoryName = "";
			%>
			<li>
				<p style="text-transform: uppercase;"><%=arr.get(i).getMainCategoryName()%></p>
				<ul>
					<%
						do {
								mainCategoryName = arr.get(i).getMainCategoryName();
								childCategoryName = arr.get(i).getChildCategoryName();
								i++;
					%>
					<li><a href="index.jsp?categoryID=<%=arr.get(i-1).getIdCategory()%>"><%=childCategoryName%></a></li>
					<%
						} while (i < arr.size() && arr.get(i).getMainCategoryName().compareToIgnoreCase(mainCategoryName) == 0);
					%>
				</ul>
			</li>
			<%
				}
			%>
		</ul>
	</div>
</body>
</html>