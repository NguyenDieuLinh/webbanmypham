package com.dieulinh.dao;

import java.sql.*;

public class ConfigJDBC {

	public static Connection initConnection() {
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/webmypham";
		String user = "nguyendieulinh";
		String pwd = "DieuLinh123";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection(url, user, pwd);
		} catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return con;
	}
}
