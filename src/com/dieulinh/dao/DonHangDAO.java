package com.dieulinh.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.dieulinh.model.DonHang;
import com.mysql.jdbc.PreparedStatement;

public class DonHangDAO {
	private Connection con = ConfigJDBC.initConnection();
	
	public void addDonHang(DonHang a){
		String query = "insert into DonHang (tennguoimua,tennguoinhan,diachimua,diachinhan,email,sodienthoai,monhangs) values (?,?,?,?,?,?,?);";
		try {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, a.getUser().getName());
			ps.setString(2, a.getTennguoinhan());
			ps.setString(3, a.getUser().getAddress());
			ps.setString(4, a.getDiachinhan());
			ps.setString(5, a.getUser().getEmail());
			ps.setString(6, a.getUser().getPhone());
			ps.setString(7, a.getGiohang().getDSHANG().toString());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
