<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đăng nhập</title>
<style>
body {
	background-color: #8eb001;
	height: 100%;
	width: 100%;
	margin: 0;
	padding: 0;
	font: normal 14px Tahoma;
}
h2 {
	color: #777;
	margin-left: 10px;
	font-weight: normal;
}

th {
	background: white;
}

td {
	padding: 10px;
}

.td {
	border: solid;
	border-width: 0.3px;
	border-color: #e6e6e6;
}

input[type=text]{
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
input[type=password] {
	padding: 5px 10px;
	border: 1px solid #ddd;
	background: #fafafa;
	float: left;
	border-radius: 2px;
}
a {
	color: #8eb001
}

.button {
	float:left;
	margin-left: calc(50% - 30px);
	margin-right: 5px;
	padding: 6px;
	background: #F3BA05;
	color: white;
	border: 1px solid #ddd;
	border-radius: 3px;
}

.button:hover {
	background: #ffffff;
	color: black;
	box-shadow: 0 0 2px rgba(35, 173, 278, 1);
}
#thanhtoan{
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
	margin-right: 15%;
	width:70%;
	float: right;
	overflow: hidden;
	border: 0;
	outline: 0;
	background-color: white;
	color: #8eb001;
	vertical-align: baseline;
	
}
.thongbao {
	width: calc(100% - 80px);
	margin: 10px 10px 20px 10px;
	padding: 9px 10px 9px 50px;
	overflow: hidden;
	zoom: 1;
	clear: left;
	border-top: 1px solid #d4ebaf;
	border-bottom: 1px solid #d4ebaf;
	background: #edfcd5 ;
	float: left;
}
</style>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="thanhtoan">
		
		<%
			
			String thongbao = (String) request.getAttribute("thongbao");
			if(thongbao==null) thongbao = "Hãy điền đầy đủ thông tin chính xác nhất!";
			
		%>
		<div class="thongbao"><%=thongbao %>
		</div>
		<form action="DangNhap" method="POST">
			<div>
				<table cellspacing='0'  align="center" 
					style="width: 100%; border-collapse: collapse; text-align:center; ">

					<!-- Table Header -->
					<thead style='background: #f6f6f6;'>
						<tr>
							<th style="width: 25%; "><h2>Đăng nhập</h2></th>
							
						</tr>
					</thead>

					<!-- Table Body -->
					<tbody>
						<tr>
							<td >
								<p>
									Tên đăng nhập <span style="color: red">*</span>
								</p> <input style="width: 26%; margin-left:37%;" align="middle" type="text"
								name="username">
							</td>
							
						</tr>
						<tr>
							<td >
								<p>
									Mật khẩu <span style="color: red">*</span>
								</p> <input  align="middle" style="width: 26%; margin-left:37%;" type="password"
								name="password">
							</td>
							
						</tr>
						
					</tbody>

				</table>

				<input type="submit" class="button"
					style="margin-bottom: 50px; margin-right: 10px;" name="dangnhap"
					value="Đăng nhập">
			</div>
		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>