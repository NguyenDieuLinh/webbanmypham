package com.dieulinh.model;

import java.util.ArrayList;

public class GioHang {
	private ArrayList<Product> giohang;

	public GioHang(){
		giohang=new ArrayList<Product>();
	}


	public void ThemMonhang(Product mh) {
		boolean finded = false;
		for(Product pro : giohang){
			if(pro.getId()==mh.getId()){
				pro.setSoluong(pro.getSoluong()+ 1);
				finded = true;
			}
		}
		

		if(!finded) {

			giohang.add(mh);

		}

	}
	public void TruMonhang(Product mh) {
		for(Product pro : giohang){
			if(pro.getId()==mh.getId()){
			
				pro.setSoluong(pro.getSoluong()-1);
				if(pro.getSoluong()<=0) giohang.remove(pro);
				break;
			}
		}

	}



	
	public ArrayList<Product> getGiohang() {

		return giohang;

	}

	public ArrayList<String> getDSHANG(){
		ArrayList<String> a = new ArrayList<>();
		for(Product p: giohang){
			a.add(p.getName());
		}
		return a;
	}
	
	public boolean XoaMonHang(int id) {
		boolean isFinded = false;
		for(Product pr : giohang){
			if(pr.getId() == id){
				isFinded = true;
				giohang.remove(pr);
				break;
			}
		}
		return isFinded;
	}

	

	public int size() {

		return giohang.size();

	}

	

	public long tongtien() {

		long t = 0;

		for (Product mh : giohang)
			
			t += mh.getTongTien();

		return t;

	}
}
